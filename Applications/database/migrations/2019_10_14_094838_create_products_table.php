<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('product_category_id')->unsigned();
            $table->text('description')->nullable();
            $table->integer('weight')->nullable();
            $table->string('photo')->nullable();
            $table->string('sku')->comment('Like code or anything!');
            $table->string('size')->nullable();
            $table->string('color')->nullable();
            $table->integer('nominal_price')->nullable();
            $table->integer('reseller_price')->nullable();
            $table->integer('whosale_price')->nullable();
            $table->integer('whosale_id')->nullable()->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
