-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: 14 Des 2019 pada 09.35
-- Versi Server: 5.7.28-0ubuntu0.18.04.4
-- PHP Version: 7.2.24-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `afsalesi_smartkids`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `billing_addresses`
--

CREATE TABLE `billing_addresses` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_reseller` int(11) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `street_address` text NOT NULL,
  `apartement` varchar(191) DEFAULT NULL,
  `city` varchar(191) NOT NULL,
  `province` varchar(191) DEFAULT NULL,
  `postal_code` int(5) DEFAULT NULL,
  `phone` varchar(191) NOT NULL,
  `email` varchar(191) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `billing_addresses`
--

INSERT INTO `billing_addresses` (`id`, `id_reseller`, `first_name`, `last_name`, `company_name`, `country`, `street_address`, `apartement`, `city`, `province`, `postal_code`, `phone`, `email`, `created_at`, `updated_at`) VALUES
(8, 26, 'Agung', 'Maulana', 'ZONE', 'Indonesia', 'Ngemplak', NULL, 'Yogyakarta', 'Jawa Tengah', 12122, '0821221212', 'agung@mail.com', '2019-11-29 17:42:22', '2019-11-29 18:33:06'),
(9, 27, 'Smart', 'Kids', NULL, NULL, 'KarangWareng', NULL, 'Cirebon', NULL, 85645, '085546616645', 'smart@mail.com', '2019-11-30 18:00:39', '2019-11-30 18:00:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `customer_category` enum('pelanggan','reseller','dropshipper') COLLATE utf8mb4_unicode_ci NOT NULL,
  `province` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_district` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `postal_code` int(11) NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `other_contact` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `customers`
--

INSERT INTO `customers` (`id`, `name`, `customer_category`, `province`, `district`, `sub_district`, `postal_code`, `phone`, `email`, `other_contact`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Test', 'dropshipper', NULL, NULL, NULL, 45156, '087999876345', 'test@mail.com', '-', '-', '2019-12-12 23:31:27', '2019-12-12 23:31:27');

-- --------------------------------------------------------

--
-- Struktur dari tabel `expenses`
--

CREATE TABLE `expenses` (
  `id` int(10) UNSIGNED NOT NULL,
  `date` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `amount` int(11) NOT NULL,
  `subtotal` int(11) NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `expenses`
--

INSERT INTO `expenses` (`id`, `date`, `name`, `price`, `amount`, `subtotal`, `note`, `created_at`, `updated_at`) VALUES
(1, '13/12/2019', 'Pembayaran Listrik Desember', 2000000, 1, 2000000, '-', '2019-12-12 23:32:49', '2019-12-12 23:33:07');

-- --------------------------------------------------------

--
-- Struktur dari tabel `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_09_01_111100_create_password_resets_table', 1),
(2, '2019_10_14_093700_create_roles_table', 1),
(3, '2019_10_14_093701_create_users_table', 1),
(4, '2019_10_14_093710_create_product_categories_table', 1),
(5, '2019_10_14_094838_create_products_table', 1),
(6, '2019_10_14_094911_create_product_stocks_table', 1),
(7, '2019_10_14_094931_create_orders_table', 1),
(8, '2019_10_14_094950_create_customers_table', 1),
(9, '2019_10_14_095007_create_expenses_table', 1),
(10, '2019_10_16_230319_create_outlets_table', 1),
(11, '2019_10_16_230734_create_suppliers_table', 1),
(12, '2019_10_16_232040_create_permissions_table', 1),
(13, '2019_10_16_232126_create_role_has_permissions_table', 1),
(14, '2019_10_27_074311_create_variant_products_table', 1);

-- --------------------------------------------------------

--
-- Struktur dari tabel `orders`
--

CREATE TABLE `orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `customer_id_1` int(10) UNSIGNED NOT NULL COMMENT 'Customer',
  `customer_id_2` int(10) UNSIGNED NOT NULL COMMENT 'Sent to',
  `supplier_id` int(10) UNSIGNED NOT NULL,
  `date_of_order` datetime NOT NULL,
  `note` text COLLATE utf8mb4_unicode_ci,
  `total_item` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `discount` int(11) DEFAULT '0',
  `price` int(11) NOT NULL,
  `be_accepted` int(11) NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `outlets`
--

CREATE TABLE `outlets` (
  `id` int(10) UNSIGNED NOT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `outlets`
--

INSERT INTO `outlets` (`id`, `logo`, `slug`, `name`, `phone`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'logo-2019-12-14 01:53:42.png', 'majalengka-menjamu', 'Majalengka Menjamu', '081383886575', 'Lingkungan Mekar Jaya No 20, RT 007 RW 003, Kelurahan Tonjong - Majalengka', NULL, '2019-10-31 23:40:23', '2019-12-13 18:53:44');

-- --------------------------------------------------------

--
-- Struktur dari tabel `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Add Order', '2019-10-31 20:25:47', '2019-10-31 20:25:47'),
(2, 'Edit Order', '2019-10-31 20:33:25', '2019-10-31 20:33:25'),
(3, 'Delete Order', '2019-10-31 20:33:29', '2019-10-31 20:33:29'),
(4, 'View Order', '2019-10-31 20:34:44', '2019-10-31 20:34:44'),
(5, 'View Product', '2019-10-31 23:12:08', '2019-10-31 23:12:08');

-- --------------------------------------------------------

--
-- Struktur dari tabel `products`
--

CREATE TABLE `products` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_category_id` int(10) UNSIGNED NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci,
  `weight` int(11) DEFAULT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sku` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL COMMENT 'Like code or anything!',
  `size` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nominal_price` int(11) DEFAULT NULL,
  `reseller_price` int(11) DEFAULT NULL,
  `whosale_price` int(11) DEFAULT NULL,
  `whosale_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `products`
--

INSERT INTO `products` (`id`, `name`, `product_category_id`, `description`, `weight`, `photo`, `sku`, `size`, `color`, `nominal_price`, `reseller_price`, `whosale_price`, `whosale_id`, `created_at`, `updated_at`) VALUES
(1, 'Super Hafiz', 1, '-', 200, 'hafiz.jpg', 'C-12343llp0', '30', 'Black', 60000, 55000, 50000, 1, '2019-10-30 17:00:00', '2019-10-30 17:00:00'),
(2, 'Roemah Boedin', 3, 'Membantu mempercepat memasak anda', 200, 'boedin.jpeg', 'C-12343llp1', NULL, NULL, 65000, 50000, 50000, 1, '2019-11-23 17:00:00', '2019-11-23 17:00:00'),
(3, 'Bee bee sari islami', 1, '-', 200, 'bee.jpeg', 'C-12343llp2', NULL, NULL, 70000, 55000, 50000, 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_categories`
--

CREATE TABLE `product_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `product_categories`
--

INSERT INTO `product_categories` (`id`, `name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Makanan', '-', '2019-10-30 13:27:38', '2019-10-30 13:27:38'),
(2, 'Pakaian', '-', '2019-10-30 13:27:47', '2019-10-30 13:27:47'),
(3, 'Mainan', '-', '2019-11-18 02:43:15', '2019-11-18 02:43:15');

-- --------------------------------------------------------

--
-- Struktur dari tabel `product_stocks`
--

CREATE TABLE `product_stocks` (
  `id` int(10) UNSIGNED NOT NULL,
  `product_id` int(10) UNSIGNED NOT NULL,
  `amount` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Struktur dari tabel `resellers`
--

CREATE TABLE `resellers` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(191) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `password` varchar(191) NOT NULL,
  `address` text NOT NULL,
  `city` varchar(191) NOT NULL,
  `postal_code` varchar(191) DEFAULT NULL,
  `phone` varchar(191) NOT NULL,
  `bank_name` varchar(191) NOT NULL,
  `bank_account` varchar(191) NOT NULL,
  `bank_account_name` varchar(191) NOT NULL,
  `ktp` varchar(191) DEFAULT NULL,
  `partners_name` varchar(191) NOT NULL,
  `office_address` text,
  `phone_recruiter` varchar(191) NOT NULL,
  `email_recruiter` varchar(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `resellers`
--

INSERT INTO `resellers` (`id`, `email`, `first_name`, `last_name`, `password`, `address`, `city`, `postal_code`, `phone`, `bank_name`, `bank_account`, `bank_account_name`, `ktp`, `partners_name`, `office_address`, `phone_recruiter`, `email_recruiter`, `created_at`, `updated_at`) VALUES
(26, 'testi@mail.com', 'Agung', 'Maulana', '$2y$10$sLS1Yh6q1Z9tBPPp7JMQuuAownvQf0x6UwaB8qx64ShoghDNkTeCO', 'Ngemplak', 'Yogyakarta', NULL, '082212131212', 'BRI', '1262681287821', 'Agung', 'agung-maulana-2019-11-30 00:42:22.jpeg', 'Astria', 'Majalengka', '089212838182', 'testi@mail.com', '2019-11-29 17:42:22', '2019-11-29 17:42:22'),
(27, 'smart@mail.com', 'Smart', 'Kids', '$2y$10$ExxoytItOMPZMgv5NhwWBeU1DYNo1xtmlfbDT5pm/vYUfpfsBV4WS', 'KarangWareng', 'Cirebon', '85645', '085546616645', 'Mandiri', '856984946', 'Smart', 'smart-kids-2019-12-01 09:00:39.jpg', 'Kids', NULL, '088458745', NULL, '2019-11-30 18:00:39', '2019-11-30 18:00:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `roles`
--

INSERT INTO `roles` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', '2019-10-31 19:37:31', '2019-10-31 19:37:31'),
(2, 'CS', '2019-10-31 19:46:44', '2019-10-31 19:47:13'),
(3, 'CS2', '2019-11-01 18:04:09', '2019-11-01 18:04:09');

-- --------------------------------------------------------

--
-- Struktur dari tabel `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`id`, `permission_id`, `role_id`, `created_at`, `updated_at`) VALUES
(24, 1, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(25, 2, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(26, 3, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(27, 4, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(28, 5, 1, '2019-10-31 23:17:53', '2019-10-31 23:17:53'),
(29, 1, 2, '2019-10-31 23:18:00', '2019-10-31 23:18:00'),
(30, 2, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(31, 3, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(32, 4, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01'),
(33, 5, 2, '2019-10-31 23:18:01', '2019-10-31 23:18:01');

-- --------------------------------------------------------

--
-- Struktur dari tabel `shipping_addresses`
--

CREATE TABLE `shipping_addresses` (
  `id` int(11) NOT NULL,
  `id_reseller` int(11) NOT NULL,
  `first_name` varchar(191) NOT NULL,
  `last_name` varchar(191) DEFAULT NULL,
  `company_name` varchar(191) DEFAULT NULL,
  `country` varchar(191) DEFAULT NULL,
  `street_address` text NOT NULL,
  `apartement` varchar(191) DEFAULT NULL,
  `city` varchar(191) NOT NULL,
  `province` varchar(191) DEFAULT NULL,
  `postal_code` int(5) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `shipping_addresses`
--

INSERT INTO `shipping_addresses` (`id`, `id_reseller`, `first_name`, `last_name`, `company_name`, `country`, `street_address`, `apartement`, `city`, `province`, `postal_code`, `created_at`, `updated_at`) VALUES
(3, 26, 'Agung', 'Maulana', 'ZONE', 'Indonesia', 'Ngemplak', NULL, 'Yogyakarta', 'Jawa Tengah', 12122, '2019-11-29 17:42:22', '2019-11-29 18:33:33'),
(4, 27, 'Smart', 'Kids', NULL, NULL, 'KarangWareng', NULL, 'Cirebon', NULL, 85645, '2019-11-30 18:00:39', '2019-11-30 18:00:39');

-- --------------------------------------------------------

--
-- Struktur dari tabel `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `delivery_location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci,
  `description` text COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `suppliers`
--

INSERT INTO `suppliers` (`id`, `name`, `delivery_location`, `phone`, `address`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Test1', 'Ds. Slangit Klangenan (Cirebon)', '081325559876', '-', '-', '2019-12-12 23:26:48', '2019-12-12 23:27:30');

-- --------------------------------------------------------

--
-- Struktur dari tabel `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `username` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) UNSIGNED NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data untuk tabel `users`
--

INSERT INTO `users` (`id`, `name`, `username`, `email`, `email_verified_at`, `password`, `photo`, `role_id`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Administrator', 'admin', 'admin@smart.com', NULL, '$2y$10$vRwcCUopFt4Bf6gjMQRPXe9MYyjohSiO3jf3FHbIIMcCpQ6s35PzS', 'administrator-2019-12-13 02:21:31.png', 1, 'hBMWeUQQ3EhUJJbYAELTfj98QgGcGE37KJmlJ4Ve2uMUdUDT5MKZHJpAcpYY', '2019-10-29 21:41:40', '2019-12-12 19:21:31'),
(2, 'User', 'user', 'user@smart.com', NULL, '$2y$10$TxTnaIPv1D41.NNWCehpLuEMCr8HUq9lt9Wj8yfFpDKm3dsqVXoIK', 'user.png', 2, 'dx94HaiubWNR4O5wpKfBZLrt2yh7ljm63xwiKzn0qi5ilii51hvL5OFAcMhl', '2019-10-29 21:41:41', '2019-10-29 21:41:41');

-- --------------------------------------------------------

--
-- Struktur dari tabel `variant_products`
--

CREATE TABLE `variant_products` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `customers_email_unique` (`email`);

--
-- Indexes for table `expenses`
--
ALTER TABLE `expenses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `outlets`
--
ALTER TABLE `outlets`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_categories`
--
ALTER TABLE `product_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `product_stocks`
--
ALTER TABLE `product_stocks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `resellers`
--
ALTER TABLE `resellers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `variant_products`
--
ALTER TABLE `variant_products`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `billing_addresses`
--
ALTER TABLE `billing_addresses`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `expenses`
--
ALTER TABLE `expenses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `outlets`
--
ALTER TABLE `outlets`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_categories`
--
ALTER TABLE `product_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `product_stocks`
--
ALTER TABLE `product_stocks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `resellers`
--
ALTER TABLE `resellers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `shipping_addresses`
--
ALTER TABLE `shipping_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `variant_products`
--
ALTER TABLE `variant_products`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
