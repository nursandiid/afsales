<?php

Route::get('/', function () {
    return redirect()->route('reseller.login.get');
});

/* --------- RESELLERS --------- */

// AUTH
Route::group(['middleware' => ['guest']], function () {
    Route::get('/resellers/login', 'Resellers\Auth\AuthController@getLogin')->name('reseller.login.get');
	Route::post('/resellers/login', 'Resellers\Auth\AuthController@postLogin')->name('reseller.login.post');

	Route::get('/resellers/register', 'Resellers\Auth\AuthController@getRegister')->name('reseller.register.get');
	Route::post('/resellers/register', 'Resellers\Auth\AuthController@postRegister')->name('reseller.register.post');
});


// Route::group(['middleware' => 'auth'], function() {
	// LOGOUT
	Route::get('/resellers/logout', 'Resellers\Auth\AuthController@logout')->name('reseller.logout.get');

	// DASHBOARD
	Route::get('/resellers/dashboard', 'Resellers\DashboardController@index')->name('reseller.dashboard.get');

	// ORDERS
	Route::get('/resellers/orders', 'Resellers\OrderController@index')->name('reseller.order.get');

	// ADDRESS
	Route::get('/resellers/address', 'Resellers\AddressController@index')->name('reseller.address.get');

	// DETAIL ACCOUNT
	Route::get('/resellers/profile', 'Resellers\ProfileController@index')->name('reseller.profile.get');

	// SHOP
	Route::get('/resellers/shop', 'Resellers\ShopController@index')->name('reseller.shop.get');
// });


/* --------- ADMIN --------- */

Route::group(['prefix' => 'admin', 'namespace' => 'Auth'], function() {
	// Authentication Routes...
	Route::get('/login', 'LoginController@showLoginForm')->name('login');
	Route::post('/login', 'LoginController@login');
	Route::post('/logout', 'LoginController@logout')->name('logout');

	// Registration Routes...
	Route::get('/register', 'RegisterController@showRegistrationForm')->name('register');
	Route::post('/register', 'RegisterController@register');
});

Route::group(['prefix'=>'admin', 'middleware'=> ['auth', 'admin']], function() {
	
	/* --------- Dashboard --------- */
	Route::get('/home', 'HomeController@index')->name('home');

	/* --------- Order / Transaction --------- */
	Route::resource('/order', 'OrderController')->except([
		'show', 'create', 'edit'
	]);
	Route::get('/order/detail/{id}', 'OrderController@show')->name('order.detail');
	Route::get('/order/search', 'OrderController@search')->name('order.search');
	Route::get('/order/new_order', 'OrderController@create')->name('order.new_order');
	Route::get('/order/edit_order/{id}', 'OrderController@edit')->name('order.edit_order');
	Route::get('/order/canceled', 'OrderController@canceled')->name('order.canceled');

	/* --------- Product Management --------- */
	Route::resource('/product', 'ProductController')->except([
		'show', 'create', 'edit'
	]);

	Route::get('/product/detail_product/{id}', 'ProductController@show')->name('product.detail');
	Route::get('/product/add_product', 'ProductController@create')->name('product.create');
	Route::get('/product/{id}/edit_product', 'ProductController@edit')->name('product.edit');
	Route::get('/product/search', 'ProductController@search')->name('product.search');
	Route::get('/product/history/{variant_id}', 'ProductController@history_variant')->name('product.history_variant');
	
	Route::get('/product/category', 'ProductCategoryController@index')->name('product_category.index');
	Route::get('/product/category/{id}/edit', 'ProductCategoryController@edit')->name('product_category.edit');
	Route::post('/product/category/store', 'ProductCategoryController@store')->name('product_category.store');
	Route::put('/product/category/{id}', 'ProductCategoryController@update')->name('product_category.update');
	Route::delete('/product/category/{id}', 'ProductCategoryController@destroy')->name('product_category.destroy');
	
	Route::get('/product/stock/listdata', 'ProductStockController@listData')->name('product_stock.data');
	Route::get('/product/stock', 'ProductStockController@index')->name('product_stock.index');
	Route::post('/product/stock', 'ProductStockController@store')->name('product_stock.store');
	Route::put('/product/stock/{id}', 'ProductStockController@update')->name('product_stock.update');
	Route::get('/product/stock/{id}/edit', 'ProductStockController@edit')->name('product_stock.edit');
	Route::delete('/product/stock/{id}', 'ProductStockController@destroy')->name('product_stock.destroy');

	/* --------- Customer --------- */
	Route::get('/customer/listdata', 'CustomerController@listData')->name('customer.data');
	Route::resource('/customer', 'CustomerController')->except([
		'show', 'create'
	]);
	Route::get('/customer/search', 'CustomerController@searchCustomer')->name('customer.search');
	// Route::get('/customer/transaction/{id}', 'TransactionController@showCustomerTransaction')->name('customer.transaction');

	/* --------- Supplier --------- */
	Route::get('/supplier/listdata', 'SupplierController@listData')->name('supplier.data');
	Route::resource('/supplier', 'SupplierController')->except([
		'show', 'create'
	]);

	/* --------- Expense --------- */
	Route::get('/expense/listdata', 'ExpenseController@listdata')->name('expense.data');
	Route::resource('/expense', 'ExpenseController')->except([
		'create'
	]);
	Route::get('/expense/search', 'ExpenseController@search')->name('expense.search');

	/* --------- Report --------- */
	Route::get('/report', 'ReportController@index')->name('report.index');
	Route::get('/report/search', 'ReportController@search')->name('report.search');
	Route::get('/report/detail/{id}', 'ReportController@show')->name('report.detail');

	/* --------- Analyzer --------- */
	Route::get('/analyzer', 'AnalyzerController@index')->name('analyzer.index');
	Route::get('/analyzer/best_seller', 'AnalyzerController@bestSeller')->name('analyzer.best_seller');
	Route::get('/analyzer/best_customer', 'AnalyzerController@bestCustomer')->name('analyzer.best_customer');
	Route::get('/analyzer/best_location', 'AnalyzerController@bestLocation')->name('analyzer.best_location');

	/* --------- Setting --------- */
	Route::get('/setting/general', 'SettingController@index')->name('setting.general');
	Route::get('/setting/supplier', 'SettingController@supplier')->name('setting.supplier');
	Route::get('/setting/user', 'SettingController@user')->name('setting.user');
	Route::get('/setting/sale_target', 'SettingController@saleTarget')->name('setting.sale_target');

	/* --------- Outlet --------- */
	Route::get('setting/general/outlet/{id}/edit', 'OutletController@edit')->name('outlet.edit');
	Route::post('setting/general/outlet', 'OutletController@insert')->name('outlet.insert');
	
	/* --------- User --------- */
	Route::resource('/role', 'RoleController')->except([
        'create', 'show'
    ]);

	/* --------- User --------- */
	Route::get('/user/listdata', 'UserController@listData')->name('user.data');
	Route::resource('/user', 'UserController')->except([
		'index', 'show'
	]);
	Route::get('/user/user_profile', 'UserController@profile')->name('user.profile');
	Route::put('/user/user_profile/{id}', 'UserController@changeProfile')->name('user.update_profile');

	Route::get('/user/roles/{id}', 'UserController@roles')->name('user.roles');
    Route::put('/user/roles/{id}', 'UserController@setRole')->name('user.set_role');
    Route::post('/user/permission', 'UserController@addPermission')->name('user.add_permission');
    Route::get('/user/role_permission', 'UserController@rolePermission')->name('user.roles_permission');
    Route::put('/user/permission/{role}', 'UserController@setRolePermission')->name('user.setRolePermission');

});