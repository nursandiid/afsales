<?php

namespace App;

use App\Model;

class Product extends Model
{
    public function product_category()
    {
        return $this->belongsTo(Product_category::class);
    }

    public function product_stocks()
    {
        return $this->hasMany(Product_stock::class);
    }
}
