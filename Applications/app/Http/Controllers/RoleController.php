<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::orderBy('created_at', 'DESC')->paginate(10);
        return view('roles.index', compact('roles'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50'
        ]);

        $role = Role::firstOrCreate(['name' => $request->name]);
        return back()->with('message', [
            'title' => 'Role berhasil ditambahkan.'
        ]);
    }

    public function edit(Role $role)
    {
        echo json_encode($role);
    }

    public function update(Role $role, Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:50'
        ]);

        $role->update(['name' => $request->name]);
        return back()->with('message', [
            'title' => 'Role berhasil diubah.'
        ]);
    }

    public function destroy(Role $role)
    {
        $role->delete();
        return back()->with('message', [
            'title' => 'Role berhasil dihapus.'
        ]);
    }
}
