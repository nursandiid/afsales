<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;
use DB;
use App\Role;
use App\Permission;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }
    
    public function listData() 
    {
        $user = User::where('role_id', '!=', 1)->orderBy('id', 'desc')->get();
        $no = 0;
        $data = array();

        foreach ($user as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->name;
            $row[] = $list->username;
            $row[] = $list->email;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-link"><i class="fas fa-pencil-alt"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-link text-danger"><i class="fas fa-trash-alt"></i></a>
                    <a href="#" class="btn btn-link text-secondary"><i class="fas fa-cog"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    public function create()
    {
        $role = Role::orderBy('name', 'ASC')->get();
        return view('users.create', compact('role'));
    }

    public function store(Request $request)
    {
        $user = new User;
        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        $user->password = bcrypt($request->password);
        $user->photo    = 'user.png';
        $user->role_id  = 2;
        $user->save();

        return response()->json([
            'message' => 'User baru berhasil ditambahkan.'
        ]);
    }

    
    public function edit($id)
    {
        $user = User::find($id);
        echo json_encode($user);
    }

    
    public function update(Request $request, User $user)
    {
        $user->name     = $request->name;
        $user->username = $request->username;
        $user->email    = $request->email;
        
        if(!empty($request->password)) {
            $user->password = bcrypt($request->password);
        }

        $user->update();
        return response()->json([
            'message' => 'User berhasil diubah.'
        ]);
    }

    
    public function destroy(User $user)
    {
        $user->delete();
        return response()->json([
            'message' => 'Data user berhasil dihapus.'
        ]);
    }

    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/users')) {
            Storage::disk('public')->makeDirectory('uploads/users');
        }
        
        $image = Image::make($image)->resize(400, 400)->save();
        Storage::disk('public')->put('uploads/users/'. $name, $image);
        
        return $name;
    }


    public function profile() 
    {
    	$users = Auth::user();
    	return view('users.profile', compact('users'));
    }


    public function changeProfile(Request $request, $id) 
    {
    	$user = User::find($id);
        $user->name = $request->name;

    	if(!empty($request->password)) {
    		if(Hash::check($request->old_password, $user->password)) {
    			$user->password = bcrypt($request->password);
    		}
    	}

    	if($request->hasFile('photo')) {
    		$image = $this->_saveFile($request->name, $request->file('photo'));

            isset($image) ? Storage::disk('public')->delete('uploads/users/'. $user->photo) : '';
    	} else $image = $user->photo;

        $user->photo = $image;
        $user->update();

        return response()->json([
            'name' => $user->name,
            'image' => $image,
            'message' => 'Data Profil berhasil diubah.'
        ]);
    }

    public function rolePermission(Request $request)
    {
        $role = $request->get('role');
        $permissions = null;
        $hasPermission = null;

        $roles = Role::all()->pluck('name');
        if (!empty($role)) {
            $getRole = Role::where('name', $role)->first();
            $hasPermission = DB::table('role_has_permissions')
                ->select('permissions.name')
                ->join('permissions', 'role_has_permissions.permission_id', '=', 'permissions.id')
                ->where('role_id', $getRole->id)->get()->pluck('name')->all();
            $permissions = Permission::all()->pluck('name');
        }

        return view('users.role_permission', compact('roles', 'permissions', 'hasPermission'));
    }


    public function addPermission(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|unique:permissions'
        ]);

        $permission = Permission::firstOrCreate([
            'name' => $request->name
        ]);
        
        return redirect()->back();
    }
    
    public function setRolePermission(Request $request, $role)
    {
        $role = Role::where('name', $role)->first();
        $role_has_permissions = DB::table('role_has_permissions')
                    ->where('role_id', $role->id)
                    ->delete();
        
        for ($i = 0; $i < count($request->permission); $i++) { 
            $permission = Permission::where('name', $request->permission[$i])->first();
            $role_has_permission = DB::table('role_has_permissions')->insert([
                'permission_id' => $permission->id,
                'role_id' => $role->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);
        }

        return back()->with('message', [
            'title' => 'Permission to Role saved!.'
        ]);
    }
}
