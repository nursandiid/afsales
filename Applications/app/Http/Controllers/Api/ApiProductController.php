<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Product_category;
use App\Product;

class ApiProductController extends Controller
{
	public function __construct()
	{
		return auth()->shouldUse('reseller');
	}
	
	public function index()
	{
		$products = Product::with('product_category')->latest()->get();
		$product_categories = Product_category::latest()->get();

		return response()->json(compact('products', 'product_categories'));
	}

	public function show($id)
    {
        $product = Product::where('id', $id)->with('product_category')->first();
        return response()->json(compact('product'));
    }
}
