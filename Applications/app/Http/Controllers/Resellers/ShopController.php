<?php

namespace App\Http\Controllers\Resellers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ShopController extends Controller
{
    public function index()
    {
        return view('resellers.shop.shop');
    }
}
