<?php

namespace App\Http\Controllers\Resellers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('resellers.profile.profile');
    }
}
