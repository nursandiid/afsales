@if ($message = session('message'))
	@push('scripts')
		<script>
			_swall('{{ $message['title'] }}', '{{ isset($message['timer']) ? $message['timer'] : 3000 }}', '{{ isset($message['type']) ? $message['type'] : 'success' }}')
		</script>
	@endpush
@endif