<table class="table table-sm table-striped table-bordered" id="{{ isset($table_id) ? $table_id : '' }}">
    <thead>
        {{ $thead }}
    </thead>
    <tbody>
        {{ $slot }}
    </tbody>
</table>