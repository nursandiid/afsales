@extends('layouts.master')

@section('title', 'Role Permission')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ route('setting.user') }}">User</a></li>
    <li class="breadcrumb-item"><a href="{{ route('role.index') }}">Role</a></li>
    <li class="breadcrumb-item active">Permission</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-4">
        @card
            @slot('title')
            <h4 class="card-title">Tambah Permission</h4>
            @endslot

            <form action="{{ route('user.add_permission') }}" method="post" novalidate class="needs-validation">
                @csrf @method('post')
                <div class="form-group">
                    <label for="name">Nama</label>
                    <input type="text" name="name" id="name" class="form-control" required autofocus>
                    <div class="invalid-feedback">
                        Masukan Role yang ingin ditambahkan.
                    </div>
                </div>
                <div class="form-group">
                    <button class="btn btn-primary">Simpan</button>
                </div>
            </form>
        @endcard
    </div>

    <div class="col-md-8">
        @card
            @slot('title')
                <h5 class="card-title">Set Permission to Role</h5>
            @endslot

            @if (session('success'))
                @alert(['type' => 'success'])
                    {{ session('success') }}
                @endalert
            @endif
            
            <form action="{{ route('user.roles_permission') }}" method="GET">
                <div class="form-group">
                    <label for="">Roles</label>
                    <div class="input-group">
                        <select name="role" class="form-control">
                            @foreach ($roles as $value)
                                <option value="{{ $value }}" {{ request()->get('role') == $value ? 'selected':'' }}>{{ $value }}</option>
                            @endforeach
                        </select>
                        <span class="input-group-append">
                            <button class="btn btn-danger">Check!</button>
                        </span>
                    </div>
                </div>
            </form>
            
            @if (!empty($permissions))
                <form action="{{ route('user.setRolePermission', request()->get('role')) }}" method="post">
                    @csrf
                    <input type="hidden" name="_method" value="PUT">
                    <div class="form-group">
                        @php $no = 1; @endphp
                        @foreach ($permissions as $key => $row)
                            <input type="checkbox" id="{{ $row }}" name="permission[]" value="{{ $row }}"
                                {{ in_array($row, $hasPermission) ? 'checked':'' }}> 
                            <label class="font-weight-normal" for="{{ $row }}">{{ $row }}</label>

                            @if ($no++%4 == 0)
                                <br>
                            @endif
                        @endforeach
                        <br>
                    </div>
                    
                    <div class="float-right">
                        <button class="btn btn-primary btn-sm"><i class="fa fa-paper-plane"></i> Set Permission</button>
                    </div>
                </form>
            @endif
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
    
</script>
@endpush