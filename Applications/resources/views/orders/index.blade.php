@extends('layouts.master')

@section('title', 'Order')

@push('css')
    <style>

        .form-control.fc_md {
            padding: 0.5rem 1rem;
            font-size: 1rem;
            line-height: 1;
        }

        .custom-control-label {
            cursor: pointer;
        }

        .tag_button .btn-outline-secondary.active,
        .tag_button .btn-outline-secondary:hover,
        .tag_button .btn-outline-secondary:focus {
            background-color: #f4f6f9;
            color: #000;
        }

        .filter label {
            font-weight: normal!important;
            font-size: .9em;
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Order</li>
@endsection

@section('main-content')
<div class="row accordion" id="accordionExample">
    <div class="col-12 text-center mb-5 tag_button">
        <button class="btn btn-md rounded-pill btn-outline-secondary mb-lg-0 mb-2">Semua Order</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Belum Bayar</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Belum Lunas</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Belum Diproses</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Belum Ada Resi</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Pengiriman Dalam Proses</button>
        <button class="btn btn-md rounded-pill btn-outline-secondary ml-lg-2 mb-lg-0 mb-2">Pengiriman Berhasil</button>
    </div>
    
    <div class="col-lg-6">
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <button class="btn btn-outline-secondary dropdown-toggle rounded-top-right-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Order ID</button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="#">Nama Customer</a>
                    <a class="dropdown-item" href="#">Nama Produk</a>
                    <a class="dropdown-item" href="#">SKU</a>
                    <div role="separator" class="dropdown-divider"></div>
                    <a class="dropdown-item" href="#">No Resi</a>
                    <a class="dropdown-item" href="#">Telp Customer</a>
                </div>
            </div>
            
            <input type="text" class="form-control fc_md" placeholder="Pencarian...">
            <div class="input-group-append">
            <button class="btn btn-outline-secondary px-3" title="Cari."><i class="fas fa-search"></i></button>
            </div>
        </div>
    </div>
    <div class="col-lg-6">
        <a href="{{ route('order.new_order') }}" class="btn btn-md btn-success float-right"><i class="fas fa-plus-circle"></i> Order Baru</a>
        <a href="#" class="btn btn-md btn-default float-right mx-2" data-toggle="collapse" data-target="#toggle_filter" aria-expanded="true" aria-controls="toggle_filter"><i class="fas fa-filter"></i> Filter</a>
        <a href="#" class="btn btn-md btn-default float-right"><i class="fas fa-download"></i> Download</a>
    </div>

    <div class="col-md-12">          
            
        <div class="collapse mt-2" id="toggle_filter" aria-labelledby="toggle_filter" data-parent="#accordionExample">
            <div class="card-body filter">
                <h3>Filter</h3>
                <div class="row">
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="pembayaran">PEMBAYARAN</label>
                        <select name="pembayaran" id="pembayaran" class="form-control">
                            <option value="all">All</option>
                            <option value="belum bayar">Belum bayar</option>
                            <option value="cicilan">Cicilan</option>
                            <option value="lunas">Lunas</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="pengiriman">PENGIRIMAN</label>
                        <select name="pengiriman" id="pengiriman" class="form-control">
                            <option value="all">All</option>
                            <option value="belum_diproses">Belum Diproses</option>
                            <option value="belum_ada_resi">Belum Ada Resi</option>
                            <option value="dalam_pengiriman">Dalam Pengiriman</option>
                            <option value="sampai_tujuan">Sampai Tujuan</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="admin">ADMIN</label>
                        <select name="admin" id="admin" class="form-control">
                            <option value="all">All</option>
                            <option value="">Admin</option>
                            <option value="">Nursandi</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="bank">BANK</label>
                        <select name="bank" id="bank" class="form-control">
                            <option value="all">All</option>
                            <option value="">BRI</option>
                            <option value="">BJB</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="kurir">KURIR</label>
                        <select name="kurir" id="kurir" class="form-control">
                            <option value="all">All</option>
                            <option value="">JNE</option>
                            <option value="">POS</option>
                            <option value="">TIKI</option>
                            <option value="">SICEPAT</option>
                            <option value="">WAHANA</option>
                            <option value="">JNT Express</option>
                            <option value="">Alfatrex</option>
                            <option value="">Lion Parsel</option>
                            <option value="">Ninja Xpress</option>
                            <option value="">SAP</option>
                            <option value="">RPX</option>
                            <option value="">COD</option>
                            <option value="">Input Manual</option>
                        </select>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="print_label">TANGGAL</label>
                        <div class="form-group">
                            <div class="btn-group">
                                <button class="btn btn-primary">Order</button>
                                <button class="btn btn-default">Bayar</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="print_label">PRINT LABEL</label>
                        <div class="form-group">
                            <div class="btn-group">
                                <button class="btn btn-primary">Semua</button>
                                <button class="btn btn-default">Printed</button>
                                <button class="btn btn-default">Unprinted</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-6">
                        <label for="print_label">RANGE TANGGAL</label>
                        <div class="row">
                            <input type="text" class="form-control datepicker col-lg-5 col-md-5 ml-lg-2 ml-sm-0">
                            <input type="text" class="form-control datepicker col-lg-5 col-md-5 ml-sm-0 ml-md-2 ml-lg-2 mt-sm-2 mt-md-0 mt-lg-0">
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-12 justify-content-center pt-3">
                        <div class="footer float-right">
                            <button class="btn btn-success btn-md">Simpan Filter</button>
                            <button class="btn btn-warning mx-2 btn-md">Hapus Filter</button>
                            <button class="btn btn-default btn-md">Gunakan Filter</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-12">
        <h5 class="card-title mt-1 mb-3">1 Order ditemukan.</h5>
    </div>
    <div class="col-md-12">
        <div class="card">
            <div class="card-header">
                <div class="row">
                    <div class="col-lg-3 col-md-3">
                        <h3>#1</h3>
                        <p class="m-0">dari App (Minggu, 27 Okt 2019)</p>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="bg-light px-2 py-2 mt-3 img-fluid rounded border">
                            <p class="m-0 p-0">-</p>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3 col-md-3">
                    
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-6">
                        <span class="d-block text-sm text-muted">PEMESAN</span>
                        <h4>Test</h4>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <span class="d-block text-sm text-muted">TOTAL BAYAR</span>
                        <div class="box rounded p-2" style="background-color: #f7d0d7;">
                            <h4 class="font-weight-bold">Rp. 114.000</h4>
                            <sup class="badge badge-danger rounded p-1 font-weight-normal">UNPAID</sup>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <span class="d-block text-sm text-muted">PRODUK (1 ITEM)</span>
                        <h5>- Celana Blue Dark x1</h5>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <span class="d-block text-sm text-muted">DIKIRIM KEPADA</span>
                        <h4>Test</h4>
                        <p class="text-muted">Admin: Nursandi</p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-6">
                        <span class="d-block text-sm text-muted">KURIR</span>
                        <img src="https://via.placeholder.com/50.png" alt="" class="rounded" width="50" height="30">
                        <img src="https://via.placeholder.com/50.png" alt="" class="rounded" width="50" height="30">
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <input type="checkbox">
                <button class="btn btn-default"><i class="fas fa-print"></i> Print</button>
                <div class="btn-group ml-2 float-right">
                    <button type="button" class="btn btn-default">Edit Order</button>
                    <button type="button" class="btn btn-default dropdown-toggle dropdown-toggle-split" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <span class="sr-only">Toggle Dropdown</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right">
                        <a class="dropdown-item" href="#">Something else here</a>
                        <a class="dropdown-item" href="#">Separated link</a>
                    </div>
                </div>
                <button class="btn btn-default float-right">Update Bayar</button>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <input type="checkbox" id="select_all"> <label for="select_all" class="text-primary">Select All</label>
        <span class="mx-1">|</span>
        <input type="checkbox" id="clear_all"> <label for="clear_all" class="text-primary">Clear All</label>

        <button class="btn btn-default mb-3 d-block"><i class="fas fa-print"></i> Print 0 Label Pengiriman.</button>
    </div>
</div>
@include('components.sweet')
@endsection


@push('scripts')
    <script>
        $('.select2').select2()
    </script>
@endpush
