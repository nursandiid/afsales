@extends('layouts.master')

@section('title', 'Produk')

@push('css')
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <style>
        .table td {
            border-bottom: 0;
        }

        .small td {
            font-size: .9em;
        }

        @media (min-width: 575.98px) {
            .table.grosir {
                width: 50%;
            }

            .side-right {
                border-right: 1px solid rgba(0, 0, 0, 0.125);
            }
        }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
    <li class="breadcrumb-item active">Detail</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-12">
        @card
            
            <div class="row">
                <div class="col-md-6 side-right">
                    <h1>Celana 
                        <small class="bg-success text-sm rounded-pill p-1 text-uppercase">Grosir</small>
                    </h1>
                    <p class="text-muted mt-3">Nyaman digunakan untuk bersantai & bekerja</p>
                </div>

                <div class="col-md-6 pl-3">
                    <a href="{{ route('product.edit', 1) }}" class="btn btn-default btn-md float-right"><i class="fas fa-edit"></i> Edit</a>
                    
                    <span class="d-block text-muted text-sm">JENIS PRODUK</span>
                    <span class="d-block font-weight-bold">{{ __('Stock Sendiri') }}</span>
                    <br>

                    <span class="d-block text-muted text-sm">KATEGORI</span>
                    <span class="d-block font-weight-bold">{{ __('Tshirt') }}</span>
                    <br>

                    <span class="d-block text-muted text-sm">TOTAL STOK</span>
                    <span class="d-block font-weight-bold">{{ __('29') }}</span>
                    <br>
                </div>

                <div class="col-md-12 table-responsive">
                    <table class="table table-hover mt-3">
                        <tr class="small">
                            <td rowspan="2" width="10%" style="padding: 0; border: 0;">
                                <img src="https://via.placeholder.com/100.png" alt="img-product" width="100" class="img-fluid shadow-sm rounded">
                            </td>
                            <td>SKU</td>
                            <td>HARGA BELI</td>
                            <td>HARGA JUAL</td>
                            <td>HARGA RESELLER</td>
                            <td>UKURAN</td>
                            <td>WARNA</td>
                            <td>BERAT</td>
                            <td>STOK</td>
                        </tr>
                        <tr>
                            <td>{{ __('P001V01') }}</td>
                            <td>{{ __('Rp. 80.000') }}</td>
                            <td>{{ __('Rp. 100.000') }}</td>
                            <td>{{ __('Rp. 95.000') }}</td>
                            <td>{{ __('L') }}</td>
                            <td>{{ __('Blue Dark') }}</td>
                            <td>{{ __('10 gr') }}</td>
                            <td>{{ __('29') }}</td>
                        </tr>
                    </table>
                </div>

                <div class="col">
                    <h5 class="card-title my-3">Harga Grosir</h5>
                    <table class="table table-sm table-bordered table-striped grosir">
                        <thead>
                            <th>Rentang</th>
                            <th>Harga Satuan</th>
                        </thead>

                        <tbody>
                            <tr>
                                <td>
                                    <span class="d-inline-block text-right" style="min-width: 20px;">2</span> 
                                    <i class="icon ion-ios-remove ml-1"></i> 
                                    <span class="d-inline-block text-right" style="min-width: 20px;">10</span> 
                                    Produk
                                </td>
                                <td>Rp. 95.000</td>
                            </tr>
                            <tr>
                                <td>
                                    <span class="d-inline-block text-right" style="min-width: 20px;">11</span>  
                                    <i class="icon ion-ios-remove ml-1"></i> 
                                    <span class="d-inline-block text-right" style="min-width: 20px;">20</span> 
                                    Produk
                                </td>
                                <td>Rp. 90.000</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            
            @slot('footer')
                <a href="{{ route('product.index') }}" class="btn btn-md btn-orange">Kembali</a>
            @endslot
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection
