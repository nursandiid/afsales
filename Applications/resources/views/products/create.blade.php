@extends('layouts.master')

@section('title', 'Produk')

@push('css')
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.css') }}">
    <link href="https://unpkg.com/ionicons@4.5.10-0/dist/css/ionicons.min.css" rel="stylesheet">
    <style>
        .table label {
            font-size: .7em;
            text-transform: uppercase;
            color: #6c757d;
        }

        .note-btn-group.btn-group.note-insert {
            display: none;
       }

       .custom-control-label {
            cursor: pointer;
            font-weight: normal!important;
       }

       .col-md-3 .nav-pills .nav-link {
            padding: .3rem 1rem;
       }
    </style>
@endpush

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item"><a href="{{ url('/products') }}">Produk</a></li>
    <li class="breadcrumb-item active">Tambah</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-lg-8 col-12">
        @card
            <form action="{{ route('product.store') }}" method="post" enctype="multipart/form-data">
                @csrf

                <input type="hidden" name="code" id="code" class="form-control" required>
                <div class="form-group row">
                    <div class="col-12">
                        <label for="name">Nama Produk</label>
                        <input type="text" name="name" id="name" class="form-control" required>
                    </div>
                </div>
                <div class="form-group">
                    <label for="description">Deskripsi</label>
                    <textarea name="description" id="description" rows="3" class="form-control summernote"></textarea>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label for="harga_normal">Detail Produk</label>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-3">
                        <div class="nav rounded border p-2 flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-general" role="tab" aria-controls="v-pills-general" aria-selected="true">General</a>
                            <a class="nav-link" id="v-pills-inventori-tab" data-toggle="pill" href="#v-pills-inventori" role="tab" aria-controls="v-pills-inventori" aria-selected="false">Inventori</a>
                            <a class="nav-link" id="v-pills-pengiriman-tab" data-toggle="pill" href="#v-pills-pengiriman" role="tab" aria-controls="v-pills-pengiriman" aria-selected="false">Pengiriman</a>
                            <a class="nav-link" id="v-pills-atribut-tab" data-toggle="pill" href="#v-pills-atribut" role="tab" aria-controls="v-pills-atribut" aria-selected="false">Atribut</a>
                        </div>
                    </div>

                    <div class="col-md-9 mt-3 mt-md-0 mt-lg-0">
                        <div class="tab-content border rounded p-2" id="v-pills-tabContent">
                            <div class="tab-pane fade show active" id="v-pills-general" role="tabpanel" aria-labelledby="v-pills-general-tab">
                                <div class="form-group row">
                                    <label for="harga_normal" class="col-sm-4 col-form-label">Harga Normal</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="harga_normal">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="harga_jual" class="col-sm-4 col-form-label">Harga Jual</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="harga_jual">
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-inventori" role="tabpanel" aria-labelledby="v-pills-inventori-tab">
                                <div class="form-group row">
                                    <label for="sku" class="col-sm-4 col-form-label">SKU</label>
                                    <div class="col-sm-8">
                                        <input type="text" class="form-control" id="sku">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kelola_stock" class="col-sm-4 col-form-label">Kelola Stok?</label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="kelola_stock" name="kelola_stock" checked>
                                        <label class="d-inline font-weight-light" for="kelola_stock">Enable stock management at product level.</label>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="jumlah_stock" class="col-sm-4 col-form-label">Jumlah Stok</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" id="jumlah_stock" value="0">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="allow_backorders" class="col-sm-4 col-form-label">Allow Backorders?</label>
                                    <div class="col-sm-8">
                                        <select name="allow_backorders" id="allow_backorders" class="custom-select custom-select-sm">
                                            <option value="">Tidak diizinkan</option>
                                            <option value="">Diizinkan, tetapi beritahu customer</option>
                                            <option value="">Diizinkan</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ambang_batas" class="col-sm-4 col-form-label">Batas</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" id="ambang_batas" value="0">
                                        <small>Ambang batas, stok tinggal sedikit</small>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="sold_individually" class="col-sm-4 col-form-label">Sold Individually</label>
                                    <div class="col-sm-8">
                                        <input type="checkbox" id="sold_individually" name="sold_individually" checked>
                                        <label class="d-inline font-weight-light" for="sold_individually">Jika diaktifkan produk hanya bisa dibeli 1 dalam sekali order.</label>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-pengiriman" role="tabpanel" aria-labelledby="v-pills-pengiriman-tab">
                                <div class="form-group row">
                                    <label for="berat" class="col-sm-4 col-form-label">Berat (kg)</label>
                                    <div class="col-sm-8">
                                        <input type="number" class="form-control" id="berat">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="length" class="col-sm-4 col-form-label">Dimensi (cm)</label>
                                    <div class="col-sm-8">
                                        <div class="form-inline">
                                            <input type="number" class="form-control col-3 mx-1" id="length" placeholder="Length">
                                            <input type="number" class="form-control col-3 mx-1" id="width" placeholder="Width">
                                            <input type="number" class="form-control col-3 mx-1" id="height" placeholder="Height">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kelas_pengiriman" class="col-sm-4 col-form-label">Kelas Pengiriman</label>
                                    <div class="col-sm-8">
                                        <select name="kelas_pengiriman" id="kelas_pengiriman" class="custom-select custom-select-sm">
                                            <option value="">Tidak ada kelas pengiriman.</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="tab-pane fade" id="v-pills-atribut" role="tabpanel" aria-labelledby="v-pills-atribut-tab">
                                <div class="form-group row">
                                    <div class="col-sm-6">
                                        <select name="atribut_kusus" id="atribut_kusus" class="custom-select custom-select-sm">
                                            <option value="">Atribut Produk Khusus</option>
                                            <option value="">-- || --</option>
                                        </select>
                                    </div>
                                    <div class="col">
                                        <button class="btn btn-outline-primary mt-2 mt-md-0 mt-lg-0">Tambah</button>
                                    </div>
                                </div>
                                
                                <strong>Title</strong>
                                <div class="form-group row">
                                    <div class="col-md-4">
                                        <small>Nama:</small>
                                        <input type="text" class="form-control" name="name" id="name">
                                    </div>
                                
                                    <div class="col-md-8">
                                        <small>Nilai:</small>
                                        <textarea name="value" id="value" class="form-control"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group mt-3">
                    <div class="col-12">
                        <label for="group">Reseller Setting</label>
                    </div>
                </div>

                <table class="table table-sm table-responsive table-bordered">
                    <thead>
                        <td>Group</td>
                        <td>Komisi Cash</td>
                        <td>Komisi Kredit</td>
                        <td>Point</td>
                        <td>
                            <button class="btn btn-link text-primary btn-sm" title="Tambah Baris"><i class="fas fa-plus-circle"></i></button>
                        </td>
                    </thead>

                    <tbody>
                        <td width="25%">
                            <select name="group" id="group" class="custom-select custom-select-sm">
                                <option value="">-- Choose -- </option>
                                <option value="">Gold</option>
                                <option value="">EPC</option>
                                <option value="">Premium</option>
                            </select>
                        </td>
                        <td>
                            <input type="text" class="form-control" name="purchase_price" id="purchase_price">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="nominal_price" id="nominal_price">
                        </td>
                        <td>
                            <input type="text" class="form-control" name="size" id="size">
                        </td>
                        <td>
                            <button type="submit" class="btn btn-link btn-sm text-danger" title="Hapus Baris" onclick="return confirm('Are you sure?')"><i class="fas fa-times-circle"></i></button>
                        </td>
                    </tbody>
                </table>

                <div class="form-group row mt-3">
                    <div class="col-12">
                        <label for="dp">DP</label>
                        <input type="number" class="form-control" name="dp" id="dp">
                    </div>
                </div>
            </form>

            @slot('footer')
                <a href="{{ route('product.index') }}" class="btn btn-orange">Kembali</a>
            @endslot
        @endcard
    </div>

    <div class="col-lg-4 col-12">
        @card
            <button class="btn btn-default">Simpan Draf</button>
            <button class="btn btn-success float-right">Terbitkan</button>
            <button class="btn btn-success float-right d-none">Perbarui</button>
        @endcard

        @card
            <div class="form-group">
                <label for="product_category_id">Kategori Produk</label>
                @foreach ($product_categories as $category)
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="{{ $category->name }}" value="{{ $category->id }}">
                        <label class="custom-control-label" for="{{ $category->name }}">{{ $category->name }}</label>
                    </div>
                @endforeach
                    <div class="custom-control custom-checkbox">
                        <input type="checkbox" class="custom-control-input" id="uncategorized" value="0">
                        <label class="custom-control-label" for="uncategorized">Uncategorized</label>
                    </div>

                <!-- / Category -->

                <p class="mt-3">
                    <a class="btn btn-link" data-toggle="collapse" href="#toggleCategory" role="button" aria-expanded="false" aria-controls="toggleCategory">
                        <u>Tambah Kategori</u>
                    </a>
                </p>
                <div class="collapse" id="toggleCategory">
                    <div class="form-group row">
                        <div class="col-8">
                            <input type="text" class="form-control" id="harga_normal">
                        </div>
                        <div class="col-4">
                            <button class="btn btn-outline-primary">Tambah Baru</button>
                        </div>
                    </div>
                </div>
            </div>
        @endcard

        @card
            <div class="form-group row">
                <label for="tag_produk" class="col-12">Tag Produk</label>
                <div class="col-8">
                    <input type="text" class="form-control" id="tag_produk">
                </div>
                <div class="col-4">
                    <button class="btn btn-outline-primary">Tambah Baru</button>
                </div>
                <div class="col mt-3">
                    <span class="badge badge-secondary rounded p-2 text-white">Test</span>
                    <span class="badge badge-secondary rounded p-2 text-white">Asd</span>
                    <span class="badge badge-secondary rounded p-2 text-white">Kdf</span>
                    <span class="badge badge-secondary rounded p-2 text-white">Absd</span>
                </div>
            </div>
        @endcard

        @card
            <div class="form-group">
                <label for="foto_produk">Foto Produk</label>
                <div class="row">
                    <div class="col">
                        <img src="https://via.placeholder.com/200.png" alt="" class="img-fluid rounded mb-2">
                        <input type="file" name="photo" class="form-control form-control-sm" style="width: 50%;" id="foto_produk">
                    </div>
                </div>
            </div>
        @endcard
    </div>
</div>
@include('components.sweet')
@endsection

@push('scripts')
    <script src="{{ asset('/AdminLTE/plugins/summernote/summernote-bs4.min.js') }}"></script>
    <script>

        $(function () {
            $('.summernote').summernote({
                'height': 150,
            })

            $('.select2').select2()
            $('.menu-master.produk').addClass('menu-open').children().addClass('active')
        })
    </script>
@endpush