<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>@yield('title')</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="icon" href="{{ asset('/images/logo.png') }}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/dist/css/adminlte.css') }}">
    <link rel="stylesheet" href="{{ asset('/AdminLTE/plugins/iCheck/square/blue.css') }}">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    <style>
        body.login-page {
            background-color: #fff;
        }

        .card {
            box-shadow: none;
        }

        .btn-orange {
            background-color: #fd7e14;
        }
        .btn-orange:hover {
            background-color: #f37308;
        }
        .btn-orange:focus {
            box-shadow: 0 0 0 0.2rem rgba(253, 147, 58, .5);
        }
    </style>
</head>
<body class="hold-transition login-page container">
    @yield('content')

    <script src="{{ asset('/AdminLTE/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('/AdminLTE/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
</body>
</html>