<aside class="main-sidebar sidebar-light-orange elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link navbar-orange" style="border-bottom: 1px solid #f37308;">
        <img src="{{ asset('/images/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle " style="height: 32px;">
        <span class="brand-text font-weight-light">Smartkids ID</span>
    </a>
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{ Storage::disk('public')->url('uploads/users') }}/{{ Auth::user()->photo }}" class="img-circle elevation-2" alt="User Image" id="img-profile">
            </div>
            <div class="info">
                <a href="{{ route('user.profile') }}" class="d-block name-profile">{{ Auth::user()->name }}</a>
            </div>
        </div>
        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link">
                        <i class="nav-icon fas fa-home"></i>
                        <p>Dashboard</p>
                    </a>
                </li>
                
                <li class="nav-header">MAIN MENU</li>
                <li class="nav-item has-treeview menu-master">
                    <a href="{{ route('order.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-cart-arrow-down"></i>
                        <p>
                            Order
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('order.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Order</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('order.canceled') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Canceled</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/order/pick-up') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Pick-up</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-item has-treeview menu-master produk">
                    <a href="{{ route('product.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-th-large"></i>
                        <p>
                            Product
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('product.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>All Product</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('product_category.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Category</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/product/tag') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Tag</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/product/attribute') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Attribute</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('product_stock.index') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Stock History</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('supplier.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-truck"></i>
                        <p>Supplier</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ url('/admin/reseller') }}" class="nav-link">
                        <i class="nav-icon fas fa-user"></i>
                        <p>Reseller</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('customer.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-user-circle"></i>
                        <p>Customer</p>
                    </a>
                </li>
                <li class="nav-item d-none d-lg-none">
                    <a href="{{ url('/payment') }}" class="nav-link">
                        <i class="nav-icon fas fa-money-bill-alt"></i>
                        <p>Payments</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('expense.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-donate"></i>
                        <p>Expense</p>
                    </a>
                </li>

                <li class="nav-header">REPORT MENU</li>
                <li class="nav-item">
                    <a href="{{ route('report.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-chart-bar"></i>
                        <p>Report</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('analyzer.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-chart-line"></i>
                        <p>Business Analysis</p>
                    </a>
                </li>

                <li class="nav-item has-treeview menu-master d-none d-lg-none">
                    <a href="{{ url('/transaction') }}" class="nav-link">
                        <i class="nav-icon fas fa-check-double"></i>
                        <p>
                            Check Mutation
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ url('/transaction') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Transaction</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('/transaction/bank') }}" class="nav-link">
                                <i class="far fa-circle nav-icon"></i>
                                <p>Bank</p>
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-header">SYSTEM</li>
                <li class="nav-item">
                    <a href="{{ route('user.profile') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Profile</p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{ route('setting.general') }}" class="nav-link">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>Outlet</p>
                    </a>
                </li>
                <li class="nav-item has-treeview menu-master">
                    <a href="{{ route('role.index') }}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            User Management
                            <i class="right fas fa-angle-left"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{ route('role.index') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Role</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('user.roles_permission') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>Role Permission</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ route('setting.user') }}" class="nav-link">
                                <i class="nav-icon far fa-circle"></i>
                                <p>User</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a href="{{ route('logout') }}" class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Sign Out</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>