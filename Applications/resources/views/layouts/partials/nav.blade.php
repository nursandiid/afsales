<nav class="main-header navbar navbar-expand bg-orange navbar-light" style="border-bottom: 1px solid #f37308">
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link d-none d-lg-block" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>

            <a class="nav-link d-lg-none d-block" data-widget="pushmenu" href="#" style="margin-left: -1em;">
                <img src="{{ asset('/images/logo.png') }}" alt="AdminLTE Logo" class="brand-image img-circle">
                <span class="brand-text d-none d-sm-block d-md-block d-lg-block" style="white-space: nowrap;">Smartkids ID</span>
            </a>
        </li>
    </ul>

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3 d-none d-lg-none">
        <div class="input-group input-group-sm">
            <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
            <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                <i class="fas fa-search"></i>
                </button>
            </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item">
            <div class="dropdown">
                <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                    <i class="fas fa-pencil-alt"></i>
                </a>
                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                    <a class="dropdown-item" href="{{ route('order.new_order') }}"><i class="far text-sm fa-circle"></i> Tambah Order</a>
                    <a class="dropdown-item" href="{{ route('product.create') }}"><i class="far text-sm fa-circle"></i> Tambah Produk</a>
                    <a class="dropdown-item" href="{{ route('customer.index') }}"><i class="far text-sm fa-circle"></i> Tambah Customer</a>
                </div>
            </div>
        </li>
        <!-- Messages Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fas fa-shopping-bag"></i>
                <span class="badge badge-danger navbar-badge"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header text-left pt-2 pb-4">
                    <span class="float-left text-sm text-muted">
                        BELUM DIBAYAR (1)
                    </span>

                    <span class="float-right text-sm">
                        <a href="{{ __('') }}">LIHAT SEMUA</a>
                    </span>
                </span>
                <div class="clearfix"></div>
                
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <h5 class="font-wight-bold text-dark"><strong>PO #1</strong> <small>- Test</small></h5>
                    <h5 class="font-wight-normal text-danger d-inline-block">Rp. 116.400</h5>
                    <span class="float-right text-muted text-sm">3 minutes ago</span>
                </a>
                <div class="dropdown-item dropdown-footer bg-white"></div>
            </div>
        </li>
        
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="fas fa-cubes"></i>
                <span class="badge badge-danger navbar-badge"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header text-left pt-2 pb-4">
                    <span class="float-left text-sm text-muted">
                        BELUM DIPROSES (0)
                    </span>

                    <span class="float-right text-sm">
                        <a href="{{ __('') }}">LIHAT SEMUA</a>
                    </span>
                </span>
                <div class="clearfix"></div>
                
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="{{ asset('/AdminLTE/dist/img') }}/user1-128x128.jpg" alt="User Avatar" class="img-size-50 mr-3 img-circle">
                        <div class="media-body">
                            <h3 class="dropdown-item-title text-dark">
                            Brad Diesel
                            <span class="float-right text-sm text-danger"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm text-dark">Call me whenever you can...</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item">
                    <!-- Message Start -->
                    <div class="media">
                        <img src="{{ asset('/AdminLTE/dist/img') }}/user8-128x128.jpg" alt="User Avatar" class="img-size-50 img-circle mr-3">
                        <div class="media-body">
                            <h3 class="dropdown-item-title text-dark">
                            John Pierce
                            <span class="float-right text-sm text-muted"><i class="fas fa-star"></i></span>
                            </h3>
                            <p class="text-sm text-dark">I got your message bro</p>
                            <p class="text-sm text-muted"><i class="far fa-clock mr-1"></i> 4 Hours Ago</p>
                        </div>
                    </div>
                    <!-- Message End -->
                </a>
                <div class="dropdown-item dropdown-footer bg-white"></div>
            </div>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#" aria-expanded="false">
                <i class="far fa-bell"></i>
                <span class="badge badge-danger navbar-badge">15</span>
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                <span class="dropdown-item dropdown-header text-left pt-2 pb-4">
                    <span class="float-left text-sm text-muted">
                        NOTIFIKASI (0)
                    </span>

                    <span class="float-right text-sm">
                        <a href="{{ __('') }}">LIHAT SEMUA</a>
                    </span>
                </span>
                <div class="clearfix"></div>

                <div class="dropdown-divider"></div>
                <a href="#" class="dropdown-item text-dark">
                    <i class="fas fa-envelope mr-2"></i> 4 new messages
                    <span class="float-right text-muted text-sm">3 mins</span>
                </a>
                <a href="#" class="dropdown-item text-dark">
                    <i class="fas fa-users mr-2"></i> 8 friend requests
                    <span class="float-right text-muted text-sm">12 hours</span>
                </a>
                <a href="#" class="dropdown-item text-dark">
                    <i class="fas fa-file mr-2"></i> 3 new reports
                    <span class="float-right text-muted text-sm">2 days</span>
                </a>
                <span class="dropdown-item dropdown-footer bg-white"></span>
            </div>
        </li>
        <a href="{{ route('logout') }}" class="nav-item nav-link" onclick="event.preventDefault(); document.getElementById('logout-form').submit()"><i class="fas fa-sign-out-alt"></i> <span class="d-none d-lg-inline-block">Sign Out</span></a>
    </ul>

    <form action="{{ route('logout') }}" method="post" class="d-none d-lg-none" id="logout-form">
        @csrf
    </form>
</nav>