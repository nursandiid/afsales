@extends('resellers.partials.layouts.index')

@section('title', 'Alamat')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Alamat</h1>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Alamat</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-3">
                                <ul class="nav nav-pills flex-column" id="myTab4" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="billing-tab4" data-toggle="tab" href="#billing4" role="tab" aria-controls="billing" aria-selected="false">Penagihan</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="shipping-tab4" data-toggle="tab" href="#shipping4" role="tab" aria-controls="shipping" aria-selected="true">Pengiriman</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-12 col-sm-12 col-md-9">
                                <div class="tab-content no-padding" id="myTab2Content">
                                    <div class="tab-pane fade active show" id="billing4" role="tabpanel" aria-labelledby="billing-tab4">
                                        <form action="" method="post">
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="first_name">Nama depan *</label>
                                                    <input id="first_name" type="text" class="form-control" name="first_name" autofocus required>
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="last_name">Nama belakang *</label>
                                                    <input id="last_name" type="text" class="form-control" name="last_name">
                                                </div>
                                            </div>
                        
                                            <div class="form-group">
                                                <label for="company_name">Nama perusahaan</label>
                                                <input id="company_name" type="text" class="form-control" name="company_name" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="country">Negara</label>
                                                <input id="country" type="text" class="form-control" name="country" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="address">Alamat *</label>
                                                <textarea name="street_address" class="form-control" id="address" cols="30" rows="100" required></textarea>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="apartment">Apartment, suite, unit, etc. (optional)</label>
                                                <input id="apartment" type="text" class="form-control" name="apartment" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-4">
                                                    <label for="city">Kota *</label>
                                                    <input id="city" type="text" class="form-control" name="city" required>
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="province">Provinsi *</label>
                                                    <input id="province" type="text" class="form-control" name="province" required>
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="postal_code">Postal Code *</label>
                                                    <input id="postal_code" type="number" class="form-control" name="postal_code" required>
                                                </div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="phone">No. Telepon *</label>
                                                    <input id="phone" type="number" class="form-control" name="phone" required>
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="email">Email *</label>
                                                    <input id="email" type="email" class="form-control" name="email" required>
                                                </div>
                                            </div>

                                            <div class="form-group text-right">
                                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                            </div>
                                        </form>
                                    </div>

                                    <div class="tab-pane fade" id="shipping4" role="tabpanel" aria-labelledby="shipping-tab4">
                                        <form action="" method="post">
                                            <div class="row">
                                                <div class="form-group col-6">
                                                    <label for="first_name">Nama depan *</label>
                                                    <input id="first_name" type="text" class="form-control" name="first_name" autofocus required>
                                                </div>
                                                <div class="form-group col-6">
                                                    <label for="last_name">Nama belakang *</label>
                                                    <input id="last_name" type="text" class="form-control" name="last_name">
                                                </div>
                                            </div>
                        
                                            <div class="form-group">
                                                <label for="company_name">Nama perusahaan</label>
                                                <input id="company_name" type="text" class="form-control" name="company_name" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="country">Negara</label>
                                                <input id="country" type="text" class="form-control" name="country" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="address">Alamat *</label>
                                                <textarea name="street_address" class="form-control" id="address" cols="30" rows="100" required></textarea>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="form-group">
                                                <label for="apartment">Apartment, suite, unit, etc. (optional)</label>
                                                <input id="apartment" type="text" class="form-control" name="apartment" required>
                                                <div class="invalid-feedback"></div>
                                            </div>

                                            <div class="row">
                                                <div class="form-group col-4">
                                                    <label for="city">Kota *</label>
                                                    <input id="city" type="text" class="form-control" name="city" required>
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="province">Provinsi *</label>
                                                    <input id="province" type="text" class="form-control" name="province" required>
                                                </div>
                                                <div class="form-group col-4">
                                                    <label for="postal_code">Postal Code *</label>
                                                    <input id="postal_code" type="number" class="form-control" name="postal_code" required>
                                                </div>
                                            </div>

                                            <div class="form-group text-right">
                                                <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection