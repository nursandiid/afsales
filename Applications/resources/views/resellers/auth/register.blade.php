@extends('resellers.partials.auth.index')

@section('title', 'Register')

@section('content')
    <div class="col-12 col-sm-10 offset-sm-1 col-md-8 offset-md-2 col-lg-8 offset-lg-2 col-xl-8 offset-xl-2">
        <div class="login-brand">
            <img src="{{ asset('images/logo.png') }}" alt="logo" width="150">
        </div>

        <div class="card card-primary">
            <div class="card-header"><h4>Daftar</h4></div>

            <div class="card-body">
                <div class="alert alert-warning alert-dismissible fade show" role="alert">
                    <strong>Akun yang sudah anda daftarkan!</strong> harus menunggu verifikasi dari admin sebelum anda <strong>login</strong>.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <form action="{{ route('reseller.register.post') }}" method="POST" enctype="multipart/form-data" class="needs-validation @if (count($errors) > 0) was-validated @endif">
                    {{ csrf_field() }}

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="first_name">Nama depan *</label>
                            <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" autofocus required>
                            @if ($errors->has('first_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('first_name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-6">
                            <label for="last_name">Nama belakang</label>
                            <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}">
                            @if ($errors->has('last_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('last_name') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email">Email *</label>
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                        @if ($errors->has('email'))
                            <div class="invalid-feedback">
                                {{ $errors->first('email') }}
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label for="password" class="d-block">Password *</label>
                            <input id="password" type="password" name="password" value="{{ old('password') }}" class="form-control pwstrength" required>
                            @if ($errors->has('password'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('password') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-6">
                            <label for="password2" class="d-block">Password Confirmation *</label>
                            <input id="password2" type="password" name="password_confirmation" value="{{ old('password_confirm') }}" class="form-control" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="address">Alamat *</label>
                        <textarea name="address" class="form-control" id="address" cols="30" rows="100" required>{{ old('address') }}</textarea>
                        @if ($errors->has('address'))
                            <div class="invalid-feedback">
                                {{ $errors->first('address') }}
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Kota *</label>
                            <input type="text" name="city" class="form-control" value="{{ old('city') }}" required>
                            @if ($errors->has('city'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('city') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-6">
                            <label>Kode Pos *</label>
                            <input type="number" name="postal_code" class="form-control" value="{{ old('postal_code') }}" required>
                            @if ($errors->has('postal_code'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('postal_code') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="phone">No. Telepon *</label>
                        <input name="phone" id="phone" type="number" value="{{ old('phone') }}" class="form-control" required>
                        @if ($errors->has('phone'))
                            <div class="invalid-feedback">
                                {{ $errors->first('phone') }}
                            </div>
                        @endif
                    </div>

                    <div class="row">
                        <div class="form-group col-4">
                            <label>Nama Bank *</label>
                            <input type="text" name="bank_name" value="{{ old('bank_name') }}" class="form-control" required>
                            @if ($errors->has('bank_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('bank_name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-4">
                            <label>No. Rekening *</label>
                            <input type="number" name="bank_account" value="{{ old('bank_account') }}" class="form-control" required>
                            @if ($errors->has('bank_account'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('bank_account') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-4">
                            <label>Nama di Rekening *</label>
                            <input type="text" name="bank_account_name" value="{{ old('bank_account_name') }}" class="form-control" required>
                            @if ($errors->has('bank_account_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('bank_account_name') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>Nama Suami/Istri *</label>
                            <input type="text" name="partners_name" value="{{ old('partners_name') }}" class="form-control" required>
                            @if ($errors->has('partners_name'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('partners_name') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-6">
                            <label>Alamat Kantor</label>
                            <input type="text" name="office_address" value="{{ old('office_address') }}" class="form-control">
                            @if ($errors->has('office_address'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('office_address') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="row">
                        <div class="form-group col-6">
                            <label>No. telepon recruiter *</label>
                            <input type="number" name="phone_recruiter" value="{{ old('phone_recruiter') }}" class="form-control" required>
                            @if ($errors->has('phone_recruiter'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('phone_recruiter') }}
                                </div>
                            @endif
                        </div>
                        <div class="form-group col-6">
                            <label>Email recruiter</label>
                            <input type="text" name="email_recruiter" value="{{ old('email_recruiter') }}" class="form-control">
                            @if ($errors->has('email_recruiter'))
                                <div class="invalid-feedback">
                                    {{ $errors->first('email_recruiter') }}
                                </div>
                            @endif
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Poto KTP *</label>
                        <input type="file" name="ktp" class="form-control" id="ktp" required>
                        @if ($errors->has('ktp'))
                            <div class="invalid-feedback">
                                {{ $errors->first('ktp') }}
                            </div>
                        @endif
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                            Daftar
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="simple-footer">
            Copyright &copy; Smartkids
        </div>
    </div>
@endsection