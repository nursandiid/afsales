@extends('resellers.partials.layouts.index')

@section('title', 'Profile')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Profile</h1>
        </div>
        <div class="row">
            <div class="col-12 col-sm-12 col-lg-8 m-auto">
                <div class="card">
                    <div class="card-header">
                        <h4>Edit Profile</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-12 col-sm-12 col-md-12">
                                <form action="" method="post">
                                    <div class="form-group">
                                        <label for="email">Email *</label>
                                        <input id="email" type="email" class="form-control" name="email" autofocus required>
                                    </div>
                                    
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="first_name">Nama depan *</label>
                                            <input id="first_name" type="text" class="form-control" name="first_name" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="last_name">Nama belakang *</label>
                                            <input id="last_name" type="text" class="form-control" name="last_name">
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="password">Password *</label>
                                        <input id="password" type="password" class="form-control" name="password" required>
                                    </div>

                                    <div class="form-group">
                                        <label for="address">Alamat *</label>
                                        <textarea name="street_address" class="form-control" id="address" cols="30" rows="100" required></textarea>
                                        <div class="invalid-feedback"></div>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label for="city">Kota *</label>
                                            <input id="city" type="text" class="form-control" name="city" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label for="postal_code">Kode Pos *</label>
                                            <input id="postal_code" type="number" class="form-control" name="postal_code" required>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="phone">No. Telepon *</label>
                                        <input id="phone" type="number" class="form-control" name="phone" required>
                                    </div>

                                    <div class="row">
                                        <div class="form-group col-4">
                                            <label>Nama Bank *</label>
                                            <input type="text" name="bank_name" class="form-control" required>
                                        </div>
                                        <div class="form-group col-4">
                                            <label>No. Rekening *</label>
                                            <input type="text" name="account_bank" class="form-control" required>
                                        </div>
                                        <div class="form-group col-4">
                                            <label>Nama di Rekening *</label>
                                            <input type="text" name="account_bank_name" class="form-control" required>
                                        </div>
                                    </div>
                    
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label>Nama Suami/Istri *</label>
                                            <input type="text" name="partner" class="form-control" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Alamat Kantor</label>
                                            <input type="text" name="office_address" class="form-control">
                                        </div>
                                    </div>
                
                                    <div class="row">
                                        <div class="form-group col-6">
                                            <label>No. telepon recruiter *</label>
                                            <input type="text" name="phone_recruiter" class="form-control" required>
                                        </div>
                                        <div class="form-group col-6">
                                            <label>Email recruiter</label>
                                            <input type="text" name="email_recruiter" class="form-control">
                                        </div>
                                    </div>
                
                                    <div class="form-group">
                                        <label>Poto KTP *</label>
                                        <input type="file" name="ktp" class="form-control" id="ktp" required>
                                    </div>

                                    <div class="form-group text-right">
                                        <button type="submit" class="btn btn-primary">Simpan Perubahan</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection