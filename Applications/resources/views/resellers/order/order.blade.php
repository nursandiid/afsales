@extends('resellers.partials.layouts.index')

@section('title', 'Order')

@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Orders</h1>
        </div>
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <div class="card-header">
                        <h4>Tabel Order</h4>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-md">
                                <tbody>
                                    <tr>
                                        <th>ORDER</th>
                                        <th>TANGGAL</th>
                                        <th>STATUS</th>
                                        <th>ITEM</th>
                                        <th>TOTAL</th>
                                        <th>AKSI</th>
                                    </tr>
                                    <tr>
                                        <td>#29991</td>
                                        <td>November 11, 2019</td>
                                        <td>On hold</td>
                                        <td>1</td>
                                        <td>Rp29,700.00</td>
                                        <td><a href="#" class="btn btn-primary">Detail</a></td>
                                    </tr>
                                    <tr>
                                        <td>#29991</td>
                                        <td>November 11, 2019</td>
                                        <td>On hold</td>
                                        <td>1</td>
                                        <td>Rp29,700.00</td>
                                        <td><a href="#" class="btn btn-primary">Detail</a></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer text-right">
                        <nav class="d-inline-block">
                        <ul class="pagination mb-0">
                            <li class="page-item disabled">
                            <a class="page-link" href="#" tabindex="-1"><i class="fas fa-chevron-left"></i></a>
                            </li>
                            <li class="page-item active"><a class="page-link" href="#">1 <span class="sr-only">(current)</span></a></li>
                            <li class="page-item">
                            <a class="page-link" href="#">2</a>
                            </li>
                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                            <li class="page-item">
                            <a class="page-link" href="#"><i class="fas fa-chevron-right"></i></a>
                            </li>
                        </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection