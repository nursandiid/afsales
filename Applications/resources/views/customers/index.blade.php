@extends('layouts.master')

@section('title', 'Customer')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Customer</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
            @slot('title')
                <a href="#" onclick="add()" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah</a>
            @endslot
            
            <form method="post"> 
                @csrf
                @table
                    @slot('thead')
                        <th width="5%">NO</th>
                        <th>Nama</th>
                        <th>Kategori</th>
                        <th>No Telpon</th>
                        <th>Alamat</th>
                        <th width="15%">Aksi</th>
                    @endslot
                @endtable
            </form>
        @endcard
    </div>
</div>
@include('customers.form')
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
    let id, table, save_method;

    jQuery(() => {

        table = $('.table').DataTable({
            'processing' : true,
            'autoWidth' : false,
            'ajax' : {
                'url' : '{{ route('customer.data') }}',
                'type' : 'GET'
            },
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups data tidak ditemukan!."
            }
        })

    })

    $(function() {
        $('#modal-form').validator().on('submit', function(e) {
            if(!e.preventDefault()) {
                id = $('#id').val();
                
                if(save_method == 'add') url = '{{ route("customer.store") }}';
                else url = 'admin/customer/' + id;

                $.ajax({
                    url : url,
                    type : 'post',
                    data : $('#modal-form form').serialize(),
                    success : function(data) {
                        $('#modal-form').modal('hide');
                        table.ajax.reload();

                        _swall(data.message);
                    }
                })

                return false;
            }
        })
    })

    function add() {
        save_method = 'add';
        $('.needs-validation').removeClass('was-validated')
        $("#modal-form").modal({
            backdrop: 'static',
            keyboard: false
        });
        $('#modal-form').modal('show');
        $('#modal-form .modal-title').html('Tambah Customer')
        $('input[name=_method]').val('POST');
        $('#modal-form form')[0].reset();
    }

    function edit(id) {
        save_method = 'edit';
        $('.needs-validation').removeClass('was-validated')
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();

        $.ajax({
            url : 'admin/customer/' + id + '/edit',
            type : 'GET',
            dataType : 'JSON',
            success : function(data) {
                $("#modal-form").modal({
                    backdrop: 'static',
                    keyboard: false
                });
                $('#modal-form').modal('show');
                $('#modal-form .modal-title').html('Edit Data Customer')
                
                $('#id').val(data.id);
                $('#customer_category').val(data.customer_category);
                $('#name').val(data.name);
                $('#province').val(data.province);
                $('#district').val(data.district);
                $('#sub_district').val(data.sub_district);
                $('#postal_code').val(data.postal_code);
                $('#phone').val(data.phone);
                $('#email').val(data.email);
                $('#other_contact').val(data.other_contact);
                $('#address').val(data.address);
            }, 
            error : function() {
                alert('Tidak dapat menampilkan data');
            }
        })
    }

    function detail(id) {
        edit(id);
        $('#modal-form input, #modal-form select, #modal-form textarea').attr('disabled', true)
        $('.footer').hide()

        $('button').on('click', function () {
            $('#modal-form input, #modal-form select, #modal-form textarea').attr('disabled', false)
            $('.footer').show()
        })
    }
 
    function _delete(id) {
        if(confirm('Apakah yakin data akan dihapus?')) {
            $.ajax({
                url : 'admin/customer/' + id,
                type : 'POST',
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data) {
                    table.ajax.reload();
                    
                    _swall(data.message);
                },
                error : function(data) {
                    alert('Tidak dapat menghapus data!');
                }
            })
        }
    }
</script>
@endpush