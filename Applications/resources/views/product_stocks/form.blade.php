@modal
    @slot('title', '')
    
    <form method="post" class="needs-validation" novalidate>
        @csrf @method('post')
        <input type="hidden" name="id" id="id">
        
        <div class="form-group">
            <label for="date_changed" class="col-form-label">Tanggal</label>
            <div class="col-md-9">
                <input type="text" class="form-control datepicker" name="date_changed" id="date_changed" required>
                <div class="invalid-feedback">
                    Masukan tanggal.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="product_id" class="col-form-label">Nama Produk</label>
            <div class="col-md-9">
                <select name="product_id" id="product_id" class="custom-select custom-select-sm" required>
                    <option disabled>-- Pilih Produk --</option>
                    @foreach ($products as $product)
                        <option value="{{ $product->id }}">{{ $product->name }}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="form-group">
            <label for="amount" class="col-form-label">Stok / Jumlah</label>
            <div class="col-md-9">
                <input type="text" class="form-control money" name="amount" id="amount" required>
                <div class="invalid-feedback">
                    Isi jumlah stok.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="description" class="col-form-label">Deskripsi</label>
            <div class="col-md-9">
                <textarea name="description" id="description" class="form-control" required>-</textarea>
            </div>
        </div>
    
        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal

@push('scripts')
    <script>
        $('.form-group').addClass('row')
        $('.col-form-label').addClass('col-md-3 font-weight-normal')
        $('.form-control').addClass('form-control-sm')
    </script>
@endpush
