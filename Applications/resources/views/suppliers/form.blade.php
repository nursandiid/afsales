@modal
    @slot('title', '')
    
    <form method="post" class="needs-validation" novalidate>
        @csrf @method('post')
        <input type="hidden" name="id" id="id">
		
		<div class="form-group">
            <div class="col-md-4">
                <label for="name" class="col-form-label">Nama Supplier</label>
                <input type="text" name="name" id="name" class="form-control" required>
                <div class="invalid-feedback">
                    Isi nama supplier.
                </div>
            </div>
            <div class="col-md-4">
                <label for="delivery_location" class="col-form-label">Lokasi Pengiriman</label>
                <input type="text" name="delivery_location" id="delivery_location" class="form-control" required>
                <div class="invalid-feedback">
                    Isi lokasi pengiriman.
                </div>
            </div>
            <div class="col-md-4">
                <label for="phone" class="col-form-label">HP / No Telpon</label>
                <input type="number" name="phone" id="phone" class="form-control" required>
                <div class="invalid-feedback">
                    Isi HP / No Telpon.
                </div>
            </div>
        </div>
		
		<div class="form-group">
            <div class="col-md-12">
                <label for="address" class="col-form-label">Alamat</label>
                <textarea name="address" id="address" class="form-control">-</textarea>
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-12">
                <label for="description" class="col-form-label">Keterangan / Catatan</label>
                <textarea name="description" id="description" class="form-control">-</textarea>
            </div>
        </div>

        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal

@push('scripts')
    <script>
        $('.form-group').addClass('row')
        $('.col-form-label').addClass('font-weight-normal')
    </script>
@endpush
