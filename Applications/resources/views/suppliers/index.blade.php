@extends('layouts.master')

@section('title', 'Supplier')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Supplier</li>
@endsection

@section('main-content')
<div class="row">
    <div class="col-md-12">
    	@card
			@slot('title')
                <a href="#" class="btn btn-primary btn-sm" onclick="add()"><i class="fa fa-plus-circle"></i> Tambah</a>
            @endslot

            @table
                @slot('thead')
	                <th width="30">No</th>
	                <th>Nama Supplier</th>
	                <th>Lokasi Pengiriman</th>
	                <th>No Telpon</th>
	                <th>Alamat</th>
	                <th width="100">Aksi</th>
			    @endslot
			@endtable
		@endcard
	</div>
</div>
@include('suppliers.form')
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
	let table, save_method;
	$(function() {
		table = $('.table').DataTable({
			'processing' : true,
			'autoWidth' : false,
			'ajax' : {
				'url' : '{{ route('supplier.data') }}',
				'type' : 'GET'
			}
		});
	})

	$(function() {
		$('#modal-form').validator().on('submit', function(e) {
			if(!e.preventDefault()) {
				let id = $('#id').val();
				
				if(save_method == 'add') url = '{{ route('supplier.store') }}';
				else url = 'admin/supplier/' + id;

				$.ajax({
					url : url,
					type : 'POST',
					data : $('#modal-form form').serialize(),
					success : function(data) {
						$('#modal-form').modal('hide');
						table.ajax.reload();

						_swall(data.message);
					}
				})

				return false;
			}
		})
	})

	function add() {
		save_method = 'add';
		$('.needs-validation').removeClass('was-validated')
        $("#modal-form").modal({
            backdrop: 'static',
            keyboard: false
        });
		$('#modal-form').modal('show');
		$('input[name=_method]').val('POST');
		$('#modal-form form')[0].reset();
		$('.modal-title').text('Tambah Supplier');
	}

	function edit(id) {
		save_method = 'edit';
		$('.needs-validation').removeClass('was-validated')
        $("#modal-form").modal({
            backdrop: 'static',
            keyboard: false
        });
		$('input[name=_method]').val('PATCH');
		$('#modal-form form')[0].reset();

		$.ajax({
			url : 'admin/supplier/' + id + '/edit',
			type : 'GET',
			dataType : 'JSON',
			success : function(data) {
				$('#modal-form').modal('show');
				$('.modal-title').text('Edit Supplier');

				$('#id').val(data.id);
				$('#name').val(data.name);
				$('#delivery_location').val(data.delivery_location);
				$('#phone').val(data.phone);
				$('#address').val(data.address);
				$('#description').val(data.description);
			}, 
			error : function() {
				alert('Tidak dapat menampilkan data');
			}
		})
	}

	function _delete(id) {
		if(confirm('Apakah yakin data akan dihapus?')) {
			$.ajax({
				url : 'admin/supplier/' + id,
				type : 'POST',
				data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
				success : function(data) {
					table.ajax.reload();
					_swall(data.message);
				},
				error : function() {
					alert('Tidak dapat menghapus data!');
				}
			})
		}
	}
</script>
@endpush