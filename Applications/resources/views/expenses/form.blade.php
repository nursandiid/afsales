@modal
    @slot('title', '')
    
    <form method="post" class="needs-validation" novalidate>
        @csrf @method('post')
        <input type="hidden" name="id" id="id">

        <div class="form-group">
            <label for="date" class="col-form-label">Tanggal</label>
            <div class="col-md-9">
                <input type="text" name="date" id="date" class="form-control datepicker" required>
                <span id="hide_date" class="d-none d-lg-none"></span>
                <div class="invalid-feedback">
                    Isi tanggal terlebih dahulu.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="name" class="col-form-label">Nama Pengeluaran</label>
            <div class="col-md-9">
                <input type="text" name="name" id="name" class="form-control" required>
                <div class="invalid-feedback">
                    Isi nama pengeluaran.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="price" class="col-form-label">Harga</label>
            <div class="col-md-9">
                <input type="text" name="price" id="price" class="form-control money" required>
                <div class="invalid-feedback">
                    Isi harga barang.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="amount" class="col-form-label">Jumlah</label>
            <div class="col-md-9">
                <input type="text" name="amount" id="amount" class="form-control money" required>
                <div class="invalid-feedback">
                    Isi jumlah barang.
                </div>
            </div>
        </div>
        <div class="form-group">
            <label for="note" class="col-form-label">Catatan</label>
            <div class="col-md-9">
                <textarea name="note" id="note" class="form-control">-</textarea>
            </div>
        </div>

        <div class="footer float-right">
            <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save</button>
        </div>
    </form>
@endmodal

@push('scripts')
    <script>
        $('.form-group').addClass('row')
        $('.col-form-label').addClass('col-md-3 font-weight-normal')
        $('.form-control').addClass('form-control-sm')
    </script>
@endpush
