@extends('layouts.master')

@section('title', 'Pengeluaran')

@section('breadcrumb')
    @parent
    <li class="breadcrumb-item active">Pengeluaran</li>
@endsection

@section('main-content')
<div class="row">

    <div class="col-md-12">
        @card
            @slot('title')
                <a href="#" class="btn btn-primary btn-sm" onclick="add()"><i class="fa fa-plus-circle"></i> Tambah</a>
            @endslot

            @table
                @slot('thead')
	                <th width="30">No</th>
	                <th>Tanggal</th>
	                <th>Nama</th>
	                <th>Harga</th>
                    <th>Jumlah</th>
                    <th>Subtotal</th>
                    <th>Note</th>
	                <th width="100">Aksi</th>
		        @endslot
            @endtable
        @endcard
    </div>
</div>
@include('expenses.form')
@endsection

@push('scripts')
<script src="{{ asset('/js/sweet.js') }}"></script>
<script src="{{ asset('/js/validate.js') }}"></script>
<script>
    let id, table, save_method;

    jQuery(() => {
        $('.datepicker').datepicker({
            'format': 'dd/mm/yyyy',
            'autoclose': true
        })

        table = $('.table').DataTable({
            'processing' : true,
            'autoWidth' : false,
            'ajax' : {
                'url' : '{{ route('expense.data') }}',
                'type' : 'GET'
            },
            "language": {
              "emptyTable": "Ups data masih kosong!.",
              "zeroRecords": "Ups Data tidak ditemukan!."
            }
        })

    })

    $(function() {
        $('#modal-form form').validator().on('submit', function(e) {
            if(!e.preventDefault()) {
                id = $('#id').val();
                
                if(save_method == 'add') url = '{{ route("expense.store") }}';
                else url = 'admin/expense/' + id;

                $.ajax({
                    url : url,
                    type : 'POST',
                    data : $('#modal-form form').serialize(),
                    success : function(data) {
                        $('#modal-form').modal('hide');
                        table.ajax.reload();
                        
                        _swall(data.message);
                    }
                })

                return false;
            }
        })
    })

    function add() {
        save_method = 'add';
        $('.needs-validation').removeClass('was-validated')
        $('#modal-form').modal('show');
        $('#modal-form .modal-title').html('Tambah Pengeluaran')
        $('input[name=_method]').val('POST');
        $('#modal-form form')[0].reset();
    }

    function edit(id) {
        save_method = 'edit';
        $('.needs-validation').removeClass('was-validated')
        $('input[name=_method]').val('PATCH');
        $('#modal-form form')[0].reset();

        $.ajax({
            url : 'admin/expense/' + id + '/edit',
            type : 'GET',
            dataType : 'JSON',
            success : function(data) {
                $('#modal-form').modal('show');
                $('#modal-form .modal-title').html('Edit Pengeluaran')

                $('#id').val(data.id);
				$('#date').val(data.date);
				$('#name').val(data.name);
                $('#price').val(data.price);
                $('#amount').val(data.amount);
                $('#note').val(data.note);

                $('#date').on('keyup keypress keydown blur', function () {
                    if ($(this).val() == '') {
                        $(this).val((data.date))
                    }
                })
            }, 
            error : function() {
                alert('Tidak dapat menampilkan data');
            }
        })
    }
 
    function _delete(id) {
        if(confirm('Apakah yakin data akan dihapus?')) {
            $.ajax({
                url : 'admin/expense/' + id,
                type : 'POST',
                data : {'_method' : 'DELETE', '_token' : $('meta[name=csrf-token]').attr('content')},
                success : function(data) {
                    table.ajax.reload();
                    
                    _swall(data.message);
                },
                error : function(data) {
                    alert('Tidak dapat menghapus data!');
                }
            })
        }
    }
</script>
@endpush